// bot.cpp : programa que controla la raqueta 1 de forma automática
// José Reimundo Díaz  52572 
#include "DatosMemCompartida.h"
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>

int main(void){

	int fdmem;
	// Declarar una variable del tipo puntero a DatosMemCompartida
	DatosMemcompartida *pdatosmemcomp;
	// Abrir el fichero
	int fd2 = open ("bot.txt",O_RDWR,0777);
	// proyctarlo en memoria
	pdatosmemcomp=(DatosMemCompartida*)mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd2, 0);
	// Cerrar el descriptor de ficheros
	close (fd2);
	while (1)
	{
		if (pdatosmemcomp->esfera.centro.y > pdatosmemcomp->raqueta1.y2) // La esfera esta por arriba
			pdatosmemcomp->accion=1;
		else
			if (pdatosmemcomp->esfera.centro.y < pdatosmemcomp->raqueta1.y1)// La esfera esta por debajo
				pdatosmemcomp->accion=-1;
			else 
				pdatosmemcomp->accion=0;

		usleep(25000);
	}
return 0;
}
